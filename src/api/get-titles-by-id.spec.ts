import getTitleSchema from '../../schema/get-titles.-schema.json';
import { expect, test } from '@playwright/test';
import Ajv from 'ajv';
const validIds = [
  {
    id: 1,
    description: 'Lot 1 on Block 1',
    ownerName: 'Jane Doe',
  },
];
const nonexistantIds = [3, 20, 9, 0];
const invalidIds = ['*', '%s', 'abc123', '%2F'];
const ajv = new Ajv();
const validate = ajv.compile(getTitleSchema);

test.describe('GET title by id', () => {
  validIds.forEach((item) => {
    test(`should return valid response with status code 200 when a valid id ${item.id} is passed`, async ({
      request,
    }) => {
      const response = await request.get(`/api/titles/${item.id}`);
      expect(await response.json()).toEqual(item);
      expect(response.ok()).toBeTruthy();
    });
  });
  test(`should return valid schema when valid title id passed`, async ({ request }) => {
    const response = await request.get(`/api/titles/1`);
    expect(response.ok()).toBeTruthy();
    expect(validate(await response.json()), { message: validate.errors?.toString() });
  });

  nonexistantIds.forEach((val) => {
    test(`should return error response 404 when a non existant id ${val} is passed`, async ({
      request,
    }) => {
      const response = await request.get(`/api/titles/${val}`);
      expect(response.status()).toEqual(404);
    });
  });
  invalidIds.forEach((val) => {
    test(`should return error response 400 when an invalid id ${val} is passed`, async ({
      request,
    }) => {
      const response = await request.get(`/api/titles/${val}`);
      expect(response.status()).toEqual(400);
    });
  });
});
