import { expect, test } from '@playwright/test';

test.describe('POST title by id', () => {
  test(`should return valid response with status when a valid request body is updated`, async ({
    request,
  }) => {
    const response = await request.post(`/api/titles/2`, { data: { ownerName: 'Bob Marley' } });
    expect(await response.json()).toEqual({
      id: 2,
      description: 'Lot 2 on Block 1',
      ownerName: 'Bob Marley',
    });
    expect(response.status()).toEqual(200);
  });

  test(`should return  not found if updating non existant id`, async ({ request }) => {
    const response = await request.post(`/api/titles/0`, { data: { ownerName: 'Bob Marley' } });
    expect(response.status()).toEqual(404);
  });
  test(`should return status 500 if updating valid id with invalid data`, async ({ request }) => {
    const response = await request.post(`/api/titles/1`, { data: { ownerSurName: 'Bob Marley' } });
    expect(response.status()).toEqual(500);
  });
  test(`should return status 400 if updating invalid with valid data`, async ({ request }) => {
    const response = await request.post(`/api/titles/**`, { data: { ownerName: 'Bob Marley' } });
    expect(response.status()).toEqual(400);
  });
});
