import test, { expect } from '@playwright/test';

test.describe('Home page', () => {
  test.beforeEach(async ({ page }) => {
    await page.goto('/');
  });
  test('displays valid heading', async ({ page }) => {
    expect(await page.locator('#root h3').innerText()).toEqual('Welcome to LandOnLite');
  });

  test('displays valid details', async ({ page }) => {
    expect(await page.locator('#root p').innerText()).toEqual(
      'You can enter a title number (e.g. "1") to view it.',
    );
  });

  test('displays valid placeholder text for input box', async ({ page }) => {
    expect(await page.locator('#root input').getAttribute('placeholder')).toEqual(
      'Enter a title number',
    );
  });

  test('displays valid text for button', async ({ page }) => {
    expect(await page.locator('#root button').innerText()).toEqual('Go');
  });

  test('redirects to land on lite page when valid title is searched', async ({ page }) => {
    await page.locator('#root input').fill('1');
    await page.locator('#root button').click();
    expect(await page.locator('#root h3').innerText()).toEqual('Title #1');
  });
});
