import test, { expect } from '@playwright/test';

test.describe('Land on lite page', () => {
  test.beforeEach(async ({ page }) => {
    await page.goto('/');
    await page.locator('#root input').fill('2');
    await page.locator('#root button').click();
    expect(await page.locator('#root h3').innerText()).toEqual('Title #2');
  });

  test('and updates owner name for title 2', async ({ page }) => {
    await page.locator('#root input[placeholder="Enter the new owner name"]').fill('Donnie darko');
    await page.locator('//button[text()="Save"]').click();
    expect(await page.locator('#root table tr:nth-child(2) td').innerText()).toEqual(
      'Donnie darko',
    );
    expect(await page.locator('#root table tr:nth-child(1) td').innerText()).toEqual(
      'Lot 2 on Block 1',
    );
  });
});
