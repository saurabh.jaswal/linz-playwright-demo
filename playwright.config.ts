// playwright.config.ts
import type { PlaywrightTestConfig } from '@playwright/test';

const config: PlaywrightTestConfig = {
  use: {
    // All requests we send go to this API endpoint.
    baseURL: 'http://localhost:3000',
    extraHTTPHeaders: {
      Accept: '*/*',
    },
    headless: false,
    launchOptions: {
      slowMo: 500,
    },
  },
  reporter: [['html', { outputFolder: 'test-reports' }]],
};
export default config;
